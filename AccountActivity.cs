﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

using Xamarin.Auth;
using Newtonsoft.Json.Linq;
using Android.Graphics;
using System.Net;

namespace Tweetty
{
	[Activity (Label = "Account Info")]			
	public class AccountActivity : Activity
	{
		Button ToMainButton;
		TextView Name;
		TextView FriendsCount;
		TextView FollowersCount;
		ImageView ImageView;

		Account account;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView(Resource.Layout.Account);

			account = ExtraUtility.GetFromExtra<Account> ("account", this);

			if(account == null)
			{
				Toast.MakeText (this, GetString (Resource.String.WentWrong), ToastLength.Long).Show ();
				Finish ();
			}

			ImageView = FindViewById<ImageView>(Resource.Id.ImageView);
			Name = FindViewById<TextView>(Resource.Id.NameView);
			FriendsCount = FindViewById<TextView>(Resource.Id.FriendsView);
			FollowersCount = FindViewById<TextView>(Resource.Id.FollowersView);

			GetAccountInfo ();

			ToMainButton = FindViewById<Button>(Resource.Id.ToMain);
			ToMainButton.Click += (object sender, EventArgs e) => Finish ();
		}

		private void GetAccountInfo()
		{
			var request = new OAuth1Request (RequestType.Get, Requests.CurrentUserInfo, null, account);

			var result = request.GetResponseAsync ().Result.GetResponseText ();

			var obj = JObject.Parse (result);

			var imageBitmap = Utilities.ImageUtility.GetBitmapFromUrl ((String)obj ["profile_image_url"]);

			ImageView.SetImageBitmap (imageBitmap);
			Name.Text = (String)obj ["name"];
			FriendsCount.Text = (String)obj ["friends_count"];
			FollowersCount.Text = (String)obj ["followers_count"];
		}
	}
}

