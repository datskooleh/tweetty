﻿using System;
using Android.Widget;
using System.Runtime.Remoting.Contexts;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.OS;
using Android.Content;
using Newtonsoft.Json;

namespace Tweetty
{
	public class TwitsListAdapter : BaseAdapter<Twitt>
	{
		private Activity activity;
		private List<Twitt> twits;

		private class ViewHolder : Java.Lang.Object
		{
			public TextView Author { get; set; }

			public ImageView AuthorImage { get; set; }

			public ImageView ContentImage { get; set; }

			public TextView Content { get; set; }
		}

		public TwitsListAdapter (Activity activity, List<Twitt> twits)
			: base()
		{
			this.activity = activity;
			this.twits = twits;
		}

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			ViewHolder holder;
			View rowView = convertView;
			// reuse views
			if (rowView == null) {
				LayoutInflater inflater = activity.LayoutInflater;
				rowView = inflater.Inflate(Resource.Layout.TwitsListItem, null);

				// configure view holder
				holder = new ViewHolder ();
				holder.Author = rowView.FindViewById<TextView>(Resource.Id.AuthorName);
				holder.AuthorImage = rowView.FindViewById<ImageView>(Resource.Id.AuthorImage);
				holder.Content = rowView.FindViewById<TextView>(Resource.Id.Content);

				rowView.Tag = holder;
			}

			holder = rowView.Tag as ViewHolder;

			Twitt twit = twits [position];

			holder.Author.Text = twit.Author;
			holder.AuthorImage.SetImageBitmap (Utilities.ImageUtility.GetBitmapFromUrl (twit.AuthorImage));
			holder.Content.Text = twit.Content;
			//holder.ContentImage.SetImageBitmap (Utilities.ImageUtility.GetBitmapFromUrl (twit.ContentImage));

			return rowView;
		}

		#region implemented abstract members of BaseAdapter


		public override long GetItemId (int position)
		{
			return twits[position].Id;
		}


		public override int Count {
			get
			{
				if (twits == null)
					return 0;
				return twits.Count;
			}
		}


		#endregion


		#region implemented abstract members of BaseAdapter


		public override Twitt this [int index] {
			get
			{
				return twits [index];
			}
		}


		#endregion

	}
}

