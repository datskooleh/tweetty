﻿using System;
using Xamarin.Social;
using Xamarin.Social.Services;
using Xamarin.Auth;

namespace Tweetty
{
	public static class OAuth
	{
		public static OAuth1Authenticator Twitter {
			get
			{ return new OAuth1Authenticator (consumerKey: "8eyQ0EGxZyFWarU7kMY9rlBiw",
			                                  consumerSecret: "gEqNlDvvBR43sNSPDe7cKNYUtmR75jjpzrR4fKwHN0jKHPpbDk",
			                                  requestTokenUrl: new Uri ("https://api.twitter.com/oauth/request_token"),
			                                  authorizeUrl: new Uri ("https://api.twitter.com/oauth/authorize"),
			                                  accessTokenUrl: new Uri ("https://api.twitter.com/oauth/access_token"),
			                                  callbackUrl: new Uri ("https://api.twitter.com/oauth2/token"));
			}
		}
	}
}

