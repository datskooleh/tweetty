﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using System.Collections.Generic;

using Xamarin.Social;
using Xamarin.Social.Services;
using Xamarin.Auth;
using Newtonsoft.Json;

namespace Tweetty
{
	[Activity (Label = "Tweetty", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		Button LogButton;
		Button WallButton;
		Button AccountButton;

		Xamarin.Auth.Account currentAccount;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			LogButton = FindViewById<Button> (Resource.Id.LogButton);
			LogButton.Click += LoginClicked;

			WallButton = FindViewById<Button> (Resource.Id.WallButton);
			WallButton.Click += WallButtonClicked;
			WallButton.Visibility = ViewStates.Invisible;

			AccountButton = FindViewById<Button> (Resource.Id.Account);
			AccountButton.Click += AccountButtonClicked;
			AccountButton.Visibility = ViewStates.Invisible;
		}

		private void CheckLogin(Xamarin.Auth.Account account)
		{
			var store = AccountStore.Create (this);

			bool mustBeSaved = true;

			foreach (var acc in store.FindAccountsForService ("Twitter"))
			{
				if (acc.Properties ["user_id"] == account.Properties ["user_id"])
				{
					mustBeSaved = false;
					break;
				}
			}

			if (mustBeSaved)
				store.Save (account, "Twitter");
		}

		private void LogoutClicked(object sender, EventArgs arg)
		{
			LogButton.Text = GetString(Resource.String.Login);

			var store = AccountStore.Create (this);

			foreach(var account in store.FindAccountsForService("Twitter"))
			        store.Delete(account, "Twitter");

			LogButton.Click -= LogoutClicked;

			LogButton.Text = GetString(Resource.String.Login);
			LogButton.Click += LoginClicked;

			WallButton.Visibility = ViewStates.Invisible;
			AccountButton.Visibility = ViewStates.Invisible;

			currentAccount = null;
		}

		private void LoginClicked(object sender, EventArgs args)
		{
			var auth = Tweetty.OAuth.Twitter;

			auth.Completed += (s, e) => {
				if(e.IsAuthenticated)
				{
					CheckLogin(e.Account);

					if(!String.IsNullOrWhiteSpace(e.Account.Properties["screen_name"]))
						Toast.MakeText(this, GetString(Resource.String.Welcome) + " " + e.Account.Properties["screen_name"], ToastLength.Long).Show();

					LogButton.Click -= LoginClicked;

					LogButton.Text = GetString(Resource.String.Logout);
					LogButton.Click += LogoutClicked;

					WallButton.Visibility = ViewStates.Visible;
					AccountButton.Visibility = ViewStates.Visible;

					currentAccount = e.Account;
				}
				else
					Toast.MakeText(this, GetString(Resource.String.LoginFailed), ToastLength.Long).Show();
			};

			var intent = auth.GetUI (this);
			StartActivity (intent);
		}

		private void AccountButtonClicked(object sender, EventArgs args)
		{
			var intent = new Intent (this, typeof(AccountActivity));

			ExtraUtility.PutIntExtra ("account", currentAccount, intent);

			StartActivity (intent);
		}

		private void WallButtonClicked(object sender, EventArgs args)
		{
			var intent = new Intent (this, typeof(WallActivity));

			ExtraUtility.PutIntExtra ("account", currentAccount, intent);

			StartActivity (intent);
		}
	}
}


