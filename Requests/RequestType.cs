﻿using System;

namespace Tweetty
{
	public static class RequestType
	{
		public static String Get { get { return "GET"; } }

		public static String Post { get { return "POST"; } }
	}
}

