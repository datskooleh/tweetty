﻿using System;

namespace Tweetty
{
	public static class Requests
	{
		static Requests ()
		{
			CurrentUserInfo = new Uri ("https://api.twitter.com/1.1/account/verify_credentials.json");

			HomeTimeline = new Uri ("https://api.twitter.com/1.1/statuses/home_timeline.json");
		}

		public static Uri CurrentUserInfo { get; private set; }

		public static Uri HomeTimeline { get; private set; }
	}
}

