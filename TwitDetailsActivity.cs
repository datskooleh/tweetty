﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Auth;

namespace Tweetty
{
	[Activity (Label = "TwitDetailsActivity")]			
	public class TwitDetailsActivity : Activity
	{
		TextView Author;
		ImageView AuthorImage;
		TextView Content;
		ImageView ContentImage;

		Twitt twit;
		Account account;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView(Resource.Layout.TwitDetails);

			twit = ExtraUtility.GetFromExtra<Twitt> ("twit", this);
			account = ExtraUtility.GetFromExtra<Account> ("account", this);

			Author = FindViewById<TextView> (Resource.Id.DetailAuthor);
			Author.Text = twit.Author;

			AuthorImage = FindViewById<ImageView> (Resource.Id.DetailImage);
			AuthorImage.SetImageBitmap (Utilities.ImageUtility.GetBitmapFromUrl(twit.AuthorImage));

			Content = FindViewById<TextView> (Resource.Id.DetailContent);
			Content.Text = twit.Content;
		}
	}
}

