﻿using System;
using Android.Content;
using Newtonsoft.Json;
using Android.App;
using Android.Widget;

namespace Tweetty
{
	public static class ExtraUtility
	{
		public static void PutIntExtra<T>(String name, T obj, Intent intent)
		{
			var serialized = JsonConvert.SerializeObject (obj);

			intent.PutExtra(name, serialized);
		}

		public static T GetFromExtra<T>(String name, Activity activity)
		{
			var obj = activity.Intent.GetStringExtra (name);

			if (String.IsNullOrWhiteSpace (obj))
			{
				Toast.MakeText (activity, activity.GetString (Resource.String.LogInFirst), ToastLength.Long).Show ();
				activity.Finish ();
			}

			return JsonConvert.DeserializeObject<T> (obj);
		}
	}
}

