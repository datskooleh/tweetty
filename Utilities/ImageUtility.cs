﻿using System;

using Newtonsoft.Json.Linq;
using Android.Graphics;
using System.Net;

namespace Tweetty.Utilities
{
	public static class ImageUtility
	{
		public static Bitmap GetBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;
			if(!(String.IsNullOrWhiteSpace(url)) && url != "null")
				using (var webClient = new WebClient())
				{
					var imageBytes = webClient.DownloadData(url);
					if (imageBytes != null && imageBytes.Length > 0)
					{
						imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
					}
				}

			return imageBitmap;
		}
	}
}

