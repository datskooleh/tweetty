﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Xamarin.Auth;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Tweetty
{
	[Activity (Label = "Twits")]			
	public class WallActivity : Activity
	{
		Button ToMain;
		Button Refresh;

		ListView PostedTwits;

		Account account;

		List<Twitt> twits;
		TwitsListAdapter adapter;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView(Resource.Layout.Wall);

			account = ExtraUtility.GetFromExtra<Account> ("account", this);

			if(account == null)
			{
				Toast.MakeText (this, GetString (Resource.String.WentWrong), ToastLength.Long).Show ();
				Finish ();
			}

			ToMain = FindViewById<Button> (Resource.Id.ToMain);
			ToMain.Click += (object sender, EventArgs e) => Finish();

			twits = new List<Twitt> ();
			adapter = new TwitsListAdapter (this, twits);

			PostedTwits = FindViewById<ListView> (Resource.Id.PostedTwits);
			PostedTwits.Adapter = adapter;

			Refresh = FindViewById<Button> (Resource.Id.Refresh);
			Refresh.Click += RefreshClicked;
			Refresh.PerformClick ();
		}

		private void RefreshClicked(object sender, EventArgs args)
		{
			Dictionary<string, string> parameters = null;

			if (twits.Count > 0)
			{
				parameters = new Dictionary<string, string> ();
				parameters.Add ("since_id", twits [0].Id.ToString());
			}
			
			var request = new OAuth1Request (RequestType.Get, Requests.HomeTimeline, parameters, account);

			var response = request.GetResponseAsync ().ContinueWith (ca => {
				var result = ca.Result;
				if (result == null)
					return;

				var objs = JArray.Parse (result.GetResponseText ());

				for(int i = objs.Count - 1; i >= 0; i--)
				{
					twits.Insert(0, new Twitt () {
						Id = Convert.ToInt64 (objs[i] ["id"]),
						Author = (String)objs[i] ["user"] ["name"],
						Content = (String)objs[i] ["text"],
						AuthorImage = (String)objs[i] ["user"] ["profile_image_url"]
					});
				}

//				foreach (var obj in objs)
//				{
//					twits.Add (new Twitt () {
//						Id = Convert.ToInt64 (obj ["id"]),
//						Author = (String)obj ["user"] ["name"],
//						Content = (String)obj ["text"],
//						AuthorImage = (String)obj ["user"] ["profile_image_url"]
//					});
//				}

				if(objs.Count > 0)
					this.RunOnUiThread(adapter.NotifyDataSetChanged);
			});

		}
	}
}

